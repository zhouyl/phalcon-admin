#!/bin/bash

# 数据库自动备份

root_dir=$(cd "$(dirname "$0")"; cd ..; pwd)
timestamp=$(date +"%y%m%d_%H%M")
backup_dir=$root_dir/data/dbbackup

# 需要备份的库
databases=(formax_crm)

# mysql 配置
mysql=/usr/local/mysql/bin/mysql
mysql_host="192.168.0.125"
mysql_user="root"
mysql_password="jsg-9898w"
mysqldump=/usr/local/mysql/bin/mysqldump

for db in ${databases[@]}; do
    file=$backup_dir/$db.$timestamp.sql
    echo -n "$db > $file ..."
    $mysqldump --force --opt --skip-lock-tables -h $mysql_host -u$mysql_user -p$mysql_password --databases $db > $file
    echo -e " \033[32;49;2mdone!\033[39;49;0m"
done
