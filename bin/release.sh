#!/usr/bin/env bash

### 项目发布脚本 ###

dir_name=$(cd "$(dirname "$0")"; pwd)
exclude_file=$dir_name/exclude.txt
webdir=$dir_name/../

branch=`git --git-dir=$dir_name/../.git branch | grep 'master'`
if [ "$branch" != '* master' ]; then
    echo 'pls switch to the "master" branch'
    exit 0
fi

if [ "$1" = '1' ]; then
    echo 'pass args would not run r.js'
elif [ "$1" = '2' ]; then
    r.js -o $webdir/app.build.js
    echo 'optimize js files completely!!!'
else
    # 发布时进行优化
    r.js -o $webdir/app.build.js optimize=none
    echo 'compact js files completely!!!'
fi

# npm install uglify-js -g
uglifyjs $webdir/public/js_src/bootup.js > $webdir/public/js/bootup.js

source_dir=$webdir
dest_dir="root@t0101.facaimao.net:/data/vhosts/crm.eformax.com/"
set_key_file="" # "-i /root/keys/t0101"
rsync $set_key_file $source_dir $dest_dir -arlpDvzh --exclude-from $exclude_file

echo 'clearing caches...'
remote_caches="\
/data/vhosts/crm.eformax.com/app/cache/* \
/data/vhosts/crm.eformax.com/app/metadata/* \
"
ssh root@hk003.facaimao.net "rm -f $remote_caches"

# clear apc cache
ssh root@t0101.facaimao.net "bash /data/vhosts/crm.eformax.com/bin/app-init.sh"

echo 'done.'
echo ' '
