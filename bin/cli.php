#!/usr/local/php-fcgi/bin/php
<?php

/**
 * 默认时区定义
 */
date_default_timezone_set('Asia/Shanghai');

/**
 * 设置错误报告模式
 */
error_reporting(E_ALL);

/**
 * 设置默认区域
 */
setlocale(LC_ALL, 'zh_CN.utf-8');

/**
 * 检测框架是否安装
 */
if (! extension_loaded('phalcon')) {
    exit('Phalcon framework extension is not installed');
}

/**
 * 检测 PDO_MYSQL
 */
if (! extension_loaded('pdo_mysql')) {
    exit('PDO_MYSQL extension is not installed');
}

/**
 * 定义项目根目录
 */
define('ROOT_PATH', dirname(__DIR__));

/**
 * Include defined constants
 */
require ROOT_PATH . '/app/config/defined.php';

/**
 * 打开/关闭错误显示
 */
ini_set('display_errors', true);

/**
 * 避免输出 html 错误信息
 */
ini_set('html_errors', false);

/**
 * 设定更大的内存限制
 */
ini_set('memory_limit', '256M');

/**
 * Include the common functions
 */
require APP_PATH . '/functions/common.php';

/**
 * Include the application functions
 */
require APP_PATH . '/functions/application.php';

/**
 * Include the CLI functions
 */
require APP_PATH . '/functions/cli.php';

/**
 * Read the configuration
 */
if (! $config = config('application')) {
    exit('Application configuration failed to load');
}

/**
 * Read auto-loader
 */
include APP_PATH . "/config/loader.php";

/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader
    ->registerDirs(
        array_merge(
            $loader->getDirs(),
            array(
                APP_PATH . '/tasks',
            )
        )
    )
    ->register();

/**
 * Using the CLI factory default services container
 */
$di = new \Phalcon\DI\FactoryDefault\CLI();

/**
 * Create an database listener
 */
$di['dbListener'] = new Formax\DbListener();

/**
 * Database connection is created based in the parameters defined in the configuration file
 *
 * @see http://docs.phalconphp.com/en/latest/reference/db.html
 * @see http://docs.phalconphp.com/en/latest/api/Phalcon_Db_Adapter_Pdo_Mysql.html
 */
foreach ($config->databases as $name => $options) {
    $key = 'db' . (($name === 'default') ? null : ".$name");

    $di[$key] = function () use ($options, $di) {
        // Create an Default Mysql connection
        $connection = new Phalcon\Db\Adapter\Pdo\Mysql($options->toArray());

        // Create an EventsManager
        $eventsManager = new Phalcon\Events\Manager();

        // Listen all the database events
        $eventsManager->attach('db', $di->get('dbListener'));

        // Assign the events manager to the connection
        $connection->setEventsManager($eventsManager);

        return $connection;
    };
}

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 *
 * @see http://docs.phalconphp.com/en/latest/reference/models.html#models-meta-data
 * @see http://docs.phalconphp.com/en/latest/api/Phalcon_Mvc_Model_MetaData.html
 */
$di['modelsMetadata'] = function () use ($config) {
    if (isset($config->models->metadata)) {
        $metaDataConfig = $config->models->metadata;
        $metadataAdapter = 'Phalcon\Mvc\Model\Metadata\\'.ucfirst($metaDataConfig->adapter);

        return new $metadataAdapter($metaDataConfig->options->toArray());
    }

    return new Phalcon\Mvc\Model\Metadata\Memory();
};

/**
 * Register the default cache component
 *
 * @see http://docs.phalconphp.com/en/latest/reference/cache.html
 */
$di['cache'] = function () use ($config, $di) {
    // Cache the files for 2 days using a Data frontend
    $frontCache = new Phalcon\Cache\Frontend\Data($config->cache->frontendOptions->toArray());

    // Create the component that will cache "Data" to a "File" backend
    $cache = new Phalcon\Cache\Backend\File($frontCache, $config->cache->backendOptions->toArray());

    return $cache;
};

/**
 * 多语言设置
 */
$di['i18n'] = function () use ($di) {
    $config  = config('application.i18n');
    $i18n = new Formax\I18n();

    $i18n
        ->addDirectory($config->directory)
        ->addAliases($config->aliases->toArray())
        ->setDefault($config->default)
        ->import($config->import->toArray());

    return $i18n;
};

/**
 * Create a console application
 */
$console = new \Phalcon\CLI\Console($di);

/**
* Process the console arguments
*/
$arguments = array(
    'task' => 'help',
    'action' => 'main',
    'params' => array(),
);
foreach($argv as $k => $arg) {
    if ($k == 1) {
        $arguments['task'] = camel($arg, false, '-');
    } elseif($k >= 2) {
        $arguments['params'][] = $arg;
    }
}

try {
    // handle incoming arguments
    $console->handle($arguments);
}
catch (\Exception $e) {
    cli_error($e->getMessage());
}
