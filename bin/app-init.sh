#!/usr/bin/env bash

### 项目自动初始化 ###

chmod +x $0
chown -R nobody.nobody $0

root_dir=$(cd "$(dirname "$0")"; cd ..; pwd)

# 需要清空的目录
rm_files=("data/cache/*" "data/metadata/*")
for file in ${rm_files[@]}; do
    file=$root_dir/$file
    rm -rf $file
    echo "Remove Files: $file"
done

# 需要创建的目录
mk_dirs=("data/logs" "data/cache" "data/metadata" "data/dbbackup" "public/files")

# 创建目录并设定权限
for dir in ${mk_dirs[@]}; do
    dir=$root_dir/$dir
    if [ ! -d $dir ]; then
        mkdir -p $dir
        echo "Created Directory: $dir"
    fi
    chown -R nobody.nobody $dir
    chmod -R 777 $dir
done

# 检测数据库配置文件
config_file=$root_dir/app/config/databases.php
if [ ! -f $config_file ]; then
    echo -e "\033[31;49;2mMissing the configuration file: $config_file\033[39;49;0m"
fi
