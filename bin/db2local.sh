
#!/bin/bash

### 数据库同步到本地 ###

all_databases=(formax_crm)
ssh='ssh root@t0101.facaimao.net'
mysqldump='/usr/local/mysql/bin/mysqldump -uroot -pjsg-9898w --single-transaction'
mysql='/usr/local/mysql/bin/mysql -uroot -pjsg-9898w -h 192.168.0.125'
create='CREATE DATABASE IF NOT EXISTS'

if [ ! $1 ]; then
    echo "Usage: \`$0 all\` or \`$0 db1 db2 ...\`"
    exit
elif [ $1 = 'all' ]; then
    databases=$all_databases
else
    databases=$*
fi

for dbname in ${databases[@]}; do
    $mysql -e "$create \`$dbname\`;"
    echo -n "dumping $dbname ..."
    $ssh $mysqldump $dbname | $mysql $dbname
    echo -e " \033[32;49;2mdone!\033[39;49;0m"
done
