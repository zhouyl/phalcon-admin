# 客户相关表设计

## 客户类型表
```sql
DROP TABLE IF EXISTS `oa_customer_types`;
CREATE TABLE `oa_customer_types` (
    `type_id`    INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '类型ID',
    `type_name`  VARCHAR(50) NOT NULL DEFAULT '' COMMENT '类型名称',
    `sort_order` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '99' COMMENT '显示排序',
    `deleted`    TINYINT(1) NOT NULL DEFAULT '0' COMMENT '已删除',
    `ctime`      INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    `mtime`      INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY      KEY (`type_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 客户信息表
```sql
DROP TABLE IF EXISTS `oa_customers`;
CREATE TABLE `oa_customers` (
    `customer_id`     INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '客户ID',
    `type_id`         INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '客户类型ID',
    `email`           VARCHAR(50) NOT NULL DEFAULT '' COMMENT '邮箱',
    `nickname`        VARCHAR(50) NOT NULL DEFAULT '' COMMENT '昵称',
    `mobile`          VARCHAR(20) NOT NULL DEFAULT '' COMMENT '手机',
    `phone`           VARCHAR(20) NOT NULL DEFAULT '' COMMENT '电话',
    `web_uid`         INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '网站UID',
    `reg_time`        INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '注册时间',
    `merchant_id`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理ID',
    `mt4_demo`        BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `mt4_live`        BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
    `country_id`      INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '国家ID',
    `last_deposited`  INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上次入金时间',
    `assign_id`       INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '分配批次ID',
    `assign_time`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '分配时间',
    `creator`         INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建者',
    `manager_user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '销售经理ID',
    `sales_user_id`   INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '销售人员ID',
    `last_followed`   INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上次跟进时间',
    `deleted`         TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '已删除',
    `ctime`           INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    `mtime`           INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY           KEY (`customer_id`),
    KEY               `IDX_type_id` (`type_id`),
    KEY               `IDX_merchant_id` (`merchant_id`),
    KEY               `IDX_country_id` (`country_id`),
    KEY               `IDX_assign_id` (`assign_id`),
    KEY               `IDX_manager_user_id` (`manager_user_id`),
    KEY               `IDX_sales_user_id` (`sales_user_id`),
    KEY               `IDX_creator` (`creator`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 客户分配批次表
```sql
DROP TABLE IF EXISTS `oa_customer_assigns`;
CREATE TABLE `oa_customer_assigns` (
    `assign_id`   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分配批次ID',
    `description` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '批次说明',
    `creator`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建人',
    `customers`   INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '客户数量',
    `ctime`       INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY       KEY (`assign_id`),
    KEY           `IDX_creator` (`creator`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 客户跟进记录表
```sql
DROP TABLE IF EXISTS `oa_customer_follow_logs`;
CREATE TABLE `oa_customer_follow_logs` (
    `log_id`         INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '日志ID',
    `customer_id`    INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '客户ID',
    `follow_user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '跟进人',
    `follow_mode`    SET('tel','qq','email','backend','msn','skype','other') NOT NULL DEFAULT '' COMMENT '跟进方式',
    `description`    VARCHAR(200) NOT NULL DEFAULT '' COMMENT '跟进说明',
    `screenshot`     VARCHAR(100) NOT NULL DEFAULT '' COMMENT '截图文件',
    `ctime`          INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY          KEY (`log_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 客户信息变更历史记录
```sql
DROP TABLE IF EXISTS `oa_customer_history`;
CREATE TABLE `oa_customer_history` (
    `id`       INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `operator` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '操作人员',
    `old_data` TEXT NOT NULL COMMENT '旧数据(序列化)',
    `new_data` TEXT NOT NULL COMMENT '新数据(序列化)',
    `remark`   VARCHAR(200) NOT NULL DEFAULT '' COMMENT '备注',
    `ctime`    INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    UNIQUE     KEY `id` (`id`),
    KEY        `IDX_operator` (`operator`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 客户详细信息表
```sql
DROP TABLE IF EXISTS `oa_customer_details`;
CREATE TABLE `oa_customer_details` (
    `customer_id`  INT(11) UNSIGNED NOT NULL COMMENT '客户ID',
    `first_name`   VARCHAR(50) NOT NULL DEFAULT '' COMMENT '名',
    `last_name`    VARCHAR(50) NOT NULL DEFAULT '' COMMENT '姓',
    `gender`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '性别(0:未知,1:女,2:男)',
    `address`      VARCHAR(200) NOT NULL DEFAULT '' COMMENT '联系地址',
    `company`      VARCHAR(200) NOT NULL DEFAULT '' COMMENT '公司',
    `industry`     VARCHAR(100) NOT NULL DEFAULT '' COMMENT '所属行业',
    `balance`      DECIMAL(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '账户余额',
    `margin_level` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '入金水平',
    `expected_ib`  INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '预期IB号',
    `im_type`      ENUM('qq','msn','skype','whatsapp','weixin','other') NOT NULL DEFAULT 'other' COMMENT 'IM类型',
    `im_account`   VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'IM账号',
    `description`  VARCHAR(500) NOT NULL DEFAULT '' COMMENT '备注信息',
    PRIMARY        KEY (`customer_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 销售小组表
```sql
DROP TABLE IF EXISTS `oa_sales_team`;
CREATE TABLE `oa_sales_team` (
    `team_id`   INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '小组ID',
    `leader`    INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '组长',
    `team_name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '小组名称',
    `ctime`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY     KEY (`team_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 销售小组成员表
```sql
DROP TABLE IF EXISTS `oa_sales`;
CREATE TABLE `oa_sales` (
    `team_id` INT(11) UNSIGNED NOT NULL COMMENT '小组ID',
    `user_id` INT(11) UNSIGNED NOT NULL COMMENT '成员ID',
    `ctime`   INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY   KEY (`team_id`,`user_id`),
    UNIQUE    KEY `UNQ_user_id` (`user_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 销售与代理对应表
```sql
DROP TABLE IF EXISTS `oa_sale_merchants`;
CREATE TABLE `oa_sale_merchants` (
    `merchant_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '代理ID',
    `user_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
    `ctime`       int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY       KEY (`merchant_id`),
    KEY           `IDX_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
