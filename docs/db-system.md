# 系统相关表设计

## 多语言数据表
```sql
DROP TABLE IF EXISTS `oa_translates`;
CREATE TABLE `oa_translates` (
    `language`   VARCHAR(10) NOT NULL COMMENT '语言类型',
    `keyword`    VARCHAR(50) NOT NULL COMMENT '标识符,例(deparment)',
    `foreign_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '关联ID',
    `translate`  VARCHAR(500) NOT NULL DEFAULT '' COMMENT '翻译内容',
    PRIMARY      KEY (`language`,`keyword`,`foreign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## 国家信息表
```sql
DROP TABLE IF EXISTS `oa_countries`;
CREATE TABLE `oa_countries` (
    `country_id`    INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '国家ID',
    `country`       VARCHAR(50) NOT NULL DEFAULT '' COMMENT '国家名称',
    `abbr`          CHAR(4) NOT NULL DEFAULT '' COMMENT '国际域名缩写',
    `area_code`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '国际电话区号',
    `time_diff`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '时差',
    `enabled`       TINYINT(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '是否启用',
    `copymaster_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'CopyMaster对应的ID',
    PRIMARY         KEY (`country_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## Session 表
```sql
DROP TABLE IF EXISTS `oa_sessions`;
CREATE TABLE `oa_sessions` (
    `session_id`  varchar(35) NOT NULL,
    `data`        text NOT NULL,
    `created_at`  int(15) unsigned NOT NULL DEFAULT '0',
    `modified_at` int(15) unsigned NOT NULL DEFAULT '0',
    PRIMARY       KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
