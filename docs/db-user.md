# OA 系统表设计

## 用户信息表
```sql
DROP TABLE IF EXISTS `oa_users`;
CREATE TABLE `oa_users` (
    `user_id`      int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `email`        varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
    `password`     varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
    `old_password` varchar(50) NOT NULL DEFAULT '',
    `first_name`   varchar(50) NOT NULL DEFAULT '' COMMENT '名',
    `last_name`    varchar(50) NOT NULL DEFAULT '' COMMENT '姓',
    `language`     varchar(10) NOT NULL DEFAULT 'en-us' COMMENT '语言',
    `reg_ip`       varchar(15) NOT NULL DEFAULT '0.0.0.0' COMMENT '注册IP',
    `reg_time`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册(通过)时间',
    `last_ip`      varchar(15) NOT NULL DEFAULT '0.0.0.0' COMMENT '上次登录IP',
    `last_time`    int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
    `last_failed`  int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录失败时间',
    `failed_nums`  smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '登录失败次数',
    `status`       enum('NORMAL','LOCKED','APPLY','REJECT','LEAVED') NOT NULL DEFAULT 'NORMAL' COMMENT '状态(NORMAL:正常,LOCKED:锁定,APPLY:申请账号,REJECT:申请被拒绝,LEAVED:离职)',
    `creator`      int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人(user_id)',
    `ctime`        int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
    `mtime`        int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY        KEY (`user_id`),
    UNIQUE         KEY `UNQ_email` (`email`),
    UNIQUE         KEY `UNQ_nickname` (`nickname`),
    KEY            `IDX_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## 用户自动登录令牌标识
```sql
DROP TABLE IF EXISTS `oa_user_tokens`;
CREATE TABLE `oa_user_tokens` (
    `user_id`     int(11) NOT NULL COMMENT '用户ID',
    `token`       varchar(32) NOT NULL DEFAULT '' COMMENT '令牌标识',
    `user_agent`  varchar(40) NOT NULL DEFAULT '' COMMENT '客户端代码',
    `expire_time` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
    `ctime`       int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY       KEY (`token`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## 部门表
```sql
DROP TABLE IF EXISTS `oa_departments`;
CREATE TABLE `oa_departments` (
    `dept_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门ID',
    `department`    varchar(50) NOT NULL DEFAULT '' COMMENT '部门',
    `parent_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父部门ID',
    `depth_path`    varchar(30) NOT NULL DEFAULT '' COMMENT '深度路径,例(2;9;)',
    `sort_order`    smallint(5) unsigned NOT NULL DEFAULT '99' COMMENT '显示排序',
    `deleted`       tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '已删除',
    `ctime`         int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
    `mtime`         int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY         KEY (`dept_id`),
    KEY             `IDX_parent_depart_id` (`parent_depart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## 部门用户表
```sql
DROP TABLE IF EXISTS `oa_department_users`;
CREATE TABLE `oa_department_users` (
    `dept_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '部门ID',
    `user_id`       int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
    `ctime`         int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY         KEY (`dept_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
