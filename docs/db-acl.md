# ACL 相关表设计

## 部门/角色结构示意图
```
|- 销售部 (oa_departments <dept_id: 17>)
|  |- 销售总监 (oa_roles <dept_id: 17, role_id: 12>)
|  |- 深圳 (oa_departments <dept_id: 99, parent_id: 17>)
|  |  |- 销售经理 (oa_roles <dept_id: 99, role_id: 18>)
|  |  |- 销售人员 (oa_roles <dept_id: 99, role_id: 19>)
|  |  |- ...
|  |- ...
|- 研发部
|  |- 测试人员
|  |- 开发人员
|     |- PHP
|     |- C++
|     |- ...
|- ...
```

## 权限资源结构示意图
```
|- 客户管理
|  |- 客户列表
|  |  |- 编辑 (customer.list.edit>)
|  |  |- 跟进 (customer.list.follow>)
|  |  |- 分配 (customer.list.assign>)
|  |  |- ...
|  |- 证明文件
|     |- 查看全部
|- ...
```

## 角色表
```sql
DROP TABLE IF EXISTS `oa_roles`;
CREATE TABLE `oa_roles` (
    `dept_id`     INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '部门ID',
    `role_id`     INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色ID',
    `protected`   TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否受到保护',
    `description` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '角色说明',
    `sort_order`  SMALLINT(5) UNSIGNED NOT NULL DEFAULT '99' COMMENT '显示排序',
    PRIMARY       KEY (`role_id`),
    KEY           `IDX_dept_id` (`dept_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 用户角色表
```sql
DROP TABLE IF EXISTS `oa_user_roles`;
CREATE TABLE `oa_user_roles` (
    `user_id` INT(11) UNSIGNED NOT NULL COMMENT '用户ID',
    `role_id` INT(11) UNSIGNED NOT NULL COMMENT '角色ID',
    PRIMARY   KEY (`user_id`,`role_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```

## 角色权限表
```sql
DROP TABLE IF EXISTS `oa_access_list`;
CREATE TABLE `oa_access_list` (
    `role_id`   INT(11) UNSIGNED NOT NULL COMMENT '角色ID',
    `resource`  VARCHAR(100) NOT NULL DEFAULT '' COMMENT '资源标识',
    `allowed`   TINYINT(1) UNSIGNED DEFAULT '0' COMMENT '是否允许',
    PRIMARY     KEY (`role_id`,`resource`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
```
