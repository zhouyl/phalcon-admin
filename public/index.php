<?php

/**
 * 默认时区定义
 */
date_default_timezone_set('Asia/Shanghai');

/**
 * 设置错误报告模式
 */
error_reporting(E_ALL);

/**
 * 设置默认区域
 */
setlocale(LC_ALL, 'zh_CN.utf-8');

/**
 * 检测框架是否安装
 */
if (! extension_loaded('phalcon')) {
    exit('Phalcon framework extension is not installed');
}

/**
 * 检测 PDO_MYSQL
 */
if (! extension_loaded('pdo_mysql')) {
    exit('PDO_MYSQL extension is not installed');
}

/**
 * 建议打开 short_open_tag
 */
if (! ini_get('short_open_tag')) {
    exit('Please modify <php.ini> and "short_open_tag" is set to "on"');
}

/**
 * 定义项目根目录
 */
define('ROOT_PATH', dirname(__DIR__));

/**
 * Include defined constants
 */
require ROOT_PATH . '/app/config/defined.php';

/**
 * 打开/关闭错误显示
 */
ini_set('display_errors', ! PRODUCTION);

/**
 * 避免 cli 或 curl 模式下 xdebug 输出 html 调试信息
 */
if (IS_CLI || IS_CURL) {
    ini_set('html_errors', false);
}

/**
 * Include the common functions
 */
require APP_PATH . '/functions/common.php';

/**
 * Include the application functions
 */
require APP_PATH . '/functions/application.php';

/**
 * Read the configuration
 */
if (! $config = config('application')) {
    exit('Application configuration failed to load');
}

/**
 * Read auto-loader
 */
include APP_PATH . "/config/loader.php";

/**
 * Read services
 */
include APP_PATH . '/config/services.php';

/**
 * Handle the request
 */
$application = new Formax\HMVCApplication($di);

/**
 * 获取输出内容
 */
echo $application->handle()->getContent();
