require([
    'setting/validator',
    'formax/common'
], function(vSetting) {

    var $loginForm = $('.login-form'),
        $forgetForm = $('.forget-form'),
        $registerForm = $('.register-form');

    // 绑定所有控件，回车自动提交表单
    $('form').each(function() {
        var $form = $(this);
        $form.find('input').keypress(function(e) {
            if (e.which == 13) {
                $form.trigger('submit');
                return false;
            }
        });
    });

    // 登录表单
    $loginForm.validator(
        $.extend(vSetting.message, {
            submit: function() {
                var $alert = $loginForm.find('.alert-danger').hide();
                $loginForm.ajaxSubmit({
                    error: $.noop,
                    success: function(data) {
                        if (data.code === 200) {
                            $alert.hide();
                            document.location.href = FORMAX.baseUrl;
                        } else {
                            $alert.show().find('.message').html(data.message);
                        }
                    }
                });
                return false;
            }
        })
    );

    // 找回密码表单
    $forgetForm.validator(
        $.extend(vSetting.message, {
            submit: function() {
                App.blockUI();
                $forgetForm.ajaxSubmit({
                    success: function(data) {
                        App.alert({
                            container: '#forget-alert',
                            type: data.code === 200 ? 'success' : 'danger',
                            message: data.message
                        });
                    },
                    complete: function() {
                        App.unblockUI();
                    }
                });
                return false;
            }
        })
    );

    $('#forget-password').click(function() {
        $loginForm.hide();
        $forgetForm.show();
    });

    $('#back-btn').click(function() {
        $loginForm.show();
        $forgetForm.hide();
    });

    $('#register-btn').click(function() {
        $loginForm.hide();
        $registerForm.show();
    });

    $('#register-back-btn').click(function() {
        $loginForm.show();
        $registerForm.hide();
    });

});
