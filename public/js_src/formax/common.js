define([
    'core/app',
    'formax/dialog',
    'formax/dom-init'
], function(App) {

    window.App = window.App || App;
    window.FORMAX = window.FORMAX || {};

    App.init();

    // 初始化 DOM
    $(document).domInit();

    // AJAX 默认处理
    $.ajaxSetup({
        cache: false,
        dataType: 'json',
        beforeSend: function(xhr) {
            App.blockUI();
        },
        error: function(xhr, status, error) {
            dialog.error(__('Network Error: ') + (error ? error : status));
        },
        complete: function() {
            App.unblockUI();
        }
    });

});
