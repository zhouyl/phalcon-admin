require.config({
    map: {
        '*': {
            'css': 'require/require-css/css' // or whatever the path to require-css is
        }
    },
    paths: {
        // jQuery
        'jquerylib': 'plugins/jquery.1.11.0',
        'jquery': 'plugins/jquery-migrate-1.2.1',
        'jquery-ui': 'plugins/jquery-ui/jquery-ui-1.10.3.custom.min',
        'jquery-slimscroll': 'plugins/jquery-slimscroll/jquery.slimscroll.min',
        'jquery-blockui': 'plugins/jquery.blockui.min',
        'jquery-cookie': 'plugins/jquery.cookie.min',
        'jquery-uniform': 'plugins/jquery-uniform/jquery.uniform',
        'jquery-datatable': 'plugins/data-tables/DT_bootstrap',
        'jquery-form': 'plugins/jquery-form/jquery.form',
        'jquery-validator': 'plugins/jquery-validator/jquery.validator',

        // Bootstrap
        'bootstrap': 'plugins/bootstrap/js/bootstrap.min',
        'bootstrap-hover-dropdown': 'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
        'bootstrap-datepicker': 'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
        'bootstrap-timepicker': 'plugins/bootstrap-timepicker/js/bootstrap-timepicker.min',
        'bootstrap-datetimepicker': 'plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker',

        // Other
        'artDialog': 'plugins/artDialog/dialog',
        'bootbox': 'plugins/bootbox/bootbox',
        'select2': 'plugins/select2/select2'
    },
    shim: {
        // jQuery
        'jquery': ['jquerylib'],
        'jquery-ui': ['jquery'],
        'jquery-slimscroll': ['jquery'],
        'jquery-blockui': ['jquery'],
        'jquery-uniform': [
            'jquery',
            'css!plugins/jquery-uniform/css/uniform.default'
        ],
        'jquery-validation': ['jquery'],
        'jquery-datatable': [
            'jquery',
            'bootstrap',
            'plugins/data-tables/jquery.dataTables',
            'css!plugins/data-tables/DT_bootstrap'
        ],
        'jquery-form': ['jquery'],
        'jquery-validator': ['jquery'],

        // Bootstrap
        'bootstrap': ['jquery'],
        'bootstrap-hover-dropdown': ['bootstrap'],
        'bootstrap-datepicker': [
            'bootstrap',
            'css!plugins/bootstrap-datepicker/css/datepicker'
        ],
        'bootstrap-timepicker': [
            'bootstrap',
            'css!plugins/bootstrap-timepicker/css/timepicker.min'
        ],
        'bootstrap-datetimepicker': [
            'bootstrap',
            'css!plugins/bootstrap-datetimepicker/css/datetimepicker'
        ],

        // Other
        'artDialog': ['jquery'],
        'bootbox': ['bootstrap'],
        'select2': [
            'jquery',
            'css!plugins/select2/select2',
            'css!plugins/select2/select2-metronic'
        ]
    },
    waitSeconds: 100 // ie load modules timeout bug fixed
});
