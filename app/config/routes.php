<?php
/**
 * 路由配置
 *
 * @see http://docs.phalconphp.com/zh/latest/reference/routing.html
 * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Mvc_Router.html
 */

$router = new Formax\Router();

// 删除多余的斜线
$router->removeExtraSlashes(true);

// 支持指定一个数字参数，例如：/article/detail/1 or /article/detail/1.html
$router->add(
    '/:controller/:action/(\d+)(\.html)?',
    array(
        'controller' => 1,
        'action'     => 2,
        'id'         => 3,
    )
);

// 默认路由，例如：/article/list or /article/list.html
$router->add(
    '/:controller/:action(\.html)?',
    array(
        'controller' => 1,
        'action'     => 2,
    )
);

// 支持不指定 action，例如：/article.html
$router->add(
    '/:controller(\.html)?',
    array(
        'controller' => 1,
        'action'     => 'index',
    )
);

return $router;
