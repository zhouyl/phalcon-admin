<?php
/**
 * @see http://docs.phalconphp.com/zh/latest/reference/db.html
 * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Db_Adapter_Pdo_Mysql.html
 */
return array(
    'default' => array(
        'adapter'  => 'Mysql',
        'host'     => '192.168.0.125',
        'username' => 'root',
        'password' => 'jsg-9898w',
        'dbname'   => 'formax_crm',
        'prefix'   => null,
        // @link http://www.php.net/manual/zh/pdo.setattribute.php
        'options'  => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // 默认以数组方式提取数据
        ),
    ),
    'e4max' => array(
        'adapter'  => 'Mysql',
        'host'     => '192.168.0.125',
        'username' => 'root',
        'password' => 'jsg-9898w',
        'dbname'   => 'e4max_user_info',
        // @link http://www.php.net/manual/zh/pdo.setattribute.php
        'options'  => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::ATTR_CASE => PDO::CASE_LOWER,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // 默认以数组方式提取数据
        ),
    ),
);
