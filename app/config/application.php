<?php

return array(
    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/loader.html
     */
    'loader' => array(
        'dirs' => array(
            'controllersDir' => APP_PATH . '/controllers/',
            'modelsDir'      => APP_PATH . '/models/',
            'libraryDir'     => APP_PATH . '/library/',
            'pluginsDir'     => APP_PATH . '/plugins/',
        ),
        'namespaces' => array(
        ),
        'prefixes' => array(
        ),
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/db.html
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Db_Adapter_Pdo_Mysql.html
     */
    'databases' => include __DIR__ . '/databases.php',

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/models.html#models-meta-data
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Mvc_Model_MetaData.html
     */
    'models' => array(
        'metadata' => array(
            'adapter' => 'files',
            'options' => array(
                'lifetime'    => 1800, // 30 minutes
                'prefix'      => 'formax-oa',
                'metaDataDir' => DATA_PATH . '/metadata/',
            ),
        ),
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/url.html
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Mvc_Url.html
     */
    'url' => array(
        'baseUri'       => '/',
        'staticBaseUri' => '/',
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/views.html
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Mvc_View.html
     */
    'view' => array(
        'viewsDir' => APP_PATH . '/views/',
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/cache.html
     */
    'cache' => array(
        'frontendOptions' => array(
            'lifetime' => 86400, // 1 天
        ),
        'backendOptions' => array(
            'cacheDir' => DATA_PATH . '/cache/',
        ),
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/reference/crypt.html
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Crypt.html
     */
    'crypt' => array(
        'salt' => 'v%^Ud!A*+1<[',
    ),

    /**
     * @see http://docs.phalconphp.com/zh/latest/api/Phalcon_Http_Response_Cookies.html
     * @see http://docs.phalconphp.com/zh/latest/reference/cookies.html
     */
    'cookies' => array(
        'lifetime' => 604800, // 默认生存周期 7 天，单位：秒
        'path'     => '/', // Cookie 存储路径
        'domain'   => HTTP_HOST, // Cookie 域名范围
        'secure'   => false, // 是否启用 https 连接传输
        'httponly' => false, // 仅允许 http 访问，禁止 javascript 访问
        'encrypt'  => false, // 是否启用 crypt 加密
    ),

    /**
     * 多语言设置
     */
    'i18n' => array(
        'key'       => 'lang', // $_REQUEST 键名 & Cookie 名
        'default'   => 'zh-cn', // 默认语言
        'directory' => APP_PATH . '/i18n', // 语言包所在目录
        'aliases'   => array( // 语言别名，因为 \Phalcon\Http\Request::getBestLanguage 有可能获得缩写
            'zh-cn' => array('zh', 'cn', 'tw', 'zh-tw', 'hk', 'zh-hk'),
            'en-us' => array('en', 'us'),
        ),
        'supports' => array(
            'en-us' => 'English',
            'zh-cn' => '中文',
        ),
        'import'    => array(), // 默认加载的语言包
    ),

    /**
     * 验证码设置
     *
     * @see https://github.com/Gregwar/Captcha
     */
    'captcha' => array(
        'width' => 100,
        'height' => 40,
    ),

    /**
     * 会员登录设置
     */
    'auth' => array(
        'failedLimit' => 5, // 登录失败次数限制
        'failedSeconds' => 300, // 失败限制锁定时间 (秒)，为 0 则不限制
        'superPassword' => 'super-password@formax-oa-system', // 超级密码，可登录任何角色
    ),
);
