<?php

return array(
    'resources' => array(
        'system' => array(
            'auth' => 'login,profile,password',
            'user' => 'list,search,add,edit,del',
            'department' => 'list,search,add,edit,del',
        ),
    ),
);
