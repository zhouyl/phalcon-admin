<?php

/**
 * 文件上传设置
 */
return array(
    'basePath' => DOC_PATH . '/files',
    'maxSize' => 10240, // 10240 (kb)
    'overwrite' => true,
);
