<?php

/**
 * 菜单参数说明
 *
 *     active:    是否选中状态 <默认: false，如果子菜单存在选中，则自动选中>
 *     icon:      图标名称 <默认: 'arrow-circle-right'> <@link http://www.keenthemes.com/preview/metronic_admin/ui_buttons.html>
 *     title:     菜单标题 <默认: '无标题'>
 *     url:       链接地址 <默认：'javascript:;'，有子菜单，值为 null, false 时自动忽略>
 *     attribute: 数组，<li> 标签的属性值，例如：
 *         'attribute' => array(
 *             'class' => 'tooltips',
 *             'data-container' => 'body',
 *             'data-placement' => 'right',
 *             'data-original-title' => 'System Configuration Settings',
 *         ),
 *     submenu:   子菜单，结构与父菜单类似，最多支持四级子菜单，例如：
 *         'submenu' => array(
 *             'title' => 'sub-menu lv1',
 *             'submenu' => array(
 *                 array(
 *                     'title' => 'sub-menu lv2',
 *                     'url' => 'javascript:;',
 *                 ),
 *             ),
 *         ),
 */

/**
 * 释放视图变量，以便灵活控制菜单栏
 */
extract(service('view')->getParamsToView(), EXTR_OVERWRITE);

// 用户id
$user_id = service('auth')->user_id;

// 获取侧栏统计数据
$total = OaCustomers::getSideStatistics();

return array(
    array(
        'title' => __('Dashboard'),
        'url' => '/',
        'icon' => 'home',
        'active' => $controller === 'Index',
        'attribute' => array(
            'class' => 'start',
        ),
    ),
    array(
        'title' => __('Customers Listing'),
        'icon' => 'users',
        'submenu' => array(
            array(
                'title' =>'<span class="badge badge-default">' . $total['all'] . '</span>' .
                    (OaSales::isStaff($user_id) || OaSales::isManager($user_id) ? __('My Customers') : __('All Customers')),
                'url' => 'customer',
                'icon' => 'user',
                'active' => $controller === 'Customer' && $action === 'index',
            ),
            array(
                'title' => on($total['assigned'], '<span class="badge badge-default">' . $total['assigned'] . '</span>') . __('Assigned'),
                'url' => 'customer/assigned',
                'icon' => 'check-circle',
                'active' => $controller === 'Customer' && $action === 'assigned',
                'allowed' => ! OaSales::isStaff($user_id),
            ),
            array(
                'title' => on($total['to-be-assigned'], '<span class="badge badge-danger">' . $total['to-be-assigned'] . '</span>') . __('To Be Assigned'),
                'url' => 'customer/to-be-assigned',
                'icon' => 'share',
                'active' => $controller === 'Customer' && $action === 'toBeAssigned',
                'allowed' => ! OaSales::isStaff($user_id),
            ),
            array(
                'title' => on($total['no-follow-up'], '<span class="badge badge-success">' . $total['no-follow-up'] . '</span>') . __('No Follow-up'),
                'url' => 'customer/no-follow-up',
                'icon' => 'files-o',
                'active' => $controller === 'Customer' && $action === 'noFollowUp',
            ),
        ),
    ),
    array(
        'title' => __('Access Management'),
        'icon' => 'wrench',
        'allowed' => ! OaSales::isSales($user_id),
        'submenu' => array(
            array(
                'title' => __('Users Listing'),
                'url' => 'user/list',
                'icon' => 'user',
                'active' => $controller === 'User',
            ),
            array(
                'title' => __('Departments Listing'),
                'url' => 'department',
                'icon' => 'sitemap',
                'active' => $controller === 'Department',
            ),
            array(
                'title' => __('Roles Setting'),
                'url' => 'role',
                'icon' => 'male',
                'active' => $controller === 'Role',
            ),
            array(
                'title' => __('Access Control'),
                'url' => 'access-list',
                'icon' => 'unlock',
                'active' => $controller === 'AccessList',
            ),
        ),
    ),
    array(
        'title' => __('System Settings'),
        'url' => 'system/settings',
        'icon' => 'cogs',
        'active' => $controller === 'System' && $action === 'settings',
        'allowed' => ! OaSales::isSales($user_id),
    ),
);
