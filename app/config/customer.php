<?php

return array(
    // 默认展示的国家
    'allowCountries' => array(0, 37, 74, 129, 172, 187),

    // 客户资料，行业分类
    'industries' => array(
        'Apparel'            => '服裝',
        'Banking'            => '银行',
        'Biotechnology'      => '生物技术',
        'Chemicals'          => '化学化工',
        'Communications'     => '通讯',
        'Construction'       => '建筑',
        'Consulting'         => '咨询',
        'Education'          => '教育',
        'Electronics'        => '电子',
        'Energy'             => '能源',
        'Engineering'        => '工程设计',
        'Entertainment'      => '文化',
        'Environmental'      => '环境保护',
        'Finance'            => '金融',
        'Government'         => '政府机构',
        'Healthcare'         => '卫生保健',
        'Hospitality'        => '医疗机构',
        'Insurance'          => '保险',
        'Machinery'          => '机械',
        'Manufacturing'      => '生产企业',
        'Media'              => '医院',
        'NotForProfit'       => '非盈利机构',
        'Recreation'         => '娱乐',
        'Retail'             => '零售',
        'Shipping'           => '海运',
        'Technology'         => '技术',
        'Telecommunications' => '电信',
        'Transportation'     => '运输',
        'Utilities'          => '公用事业',
        'Other'              => '其它',
    ),

    // 客户资料，IM 类型
    'im' => array(
        'qq'       => 'QQ',
        'msn'      => 'MSN',
        'skype'    => 'Skype',
        'whatsapp' => 'WhatsApp',
        'weixin'   => '微信',
        'other'    => '其它',
    ),
);
