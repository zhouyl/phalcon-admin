<?php

class OaUserTokens extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $token;

    /**
     *
     * @var string
     */
    public $user_agent = '';

    /**
     *
     * @var integer
     */
    public $expire_time = 0;

    /**
     *
     * @var integer
     */
    public $ctime = 0;

}
