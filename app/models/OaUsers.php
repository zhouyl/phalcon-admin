<?php

class OaUsers extends \Formax\Model
{

    const NORMAL = 'NORMAL';
    const LOCKED = 'LOCKED';
    const APPLY  = 'APPLY';
    const REJECT = 'REJECT';
    const LEAVED = 'LEAVED';

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $email = '';

    /**
     *
     * @var string
     */
    public $password = '';

    /**
     *
     * @var string
     */
    public $old_password = '';

    /**
     *
     * @var string
     */
    public $first_name = '';

    /**
     *
     * @var string
     */
    public $last_name = '';

    /**
     *
     * @var string
     */
    public $language = 'cn';

    /**
     *
     * @var string
     */
    public $reg_ip = '0.0.0.0';

    /**
     *
     * @var integer
     */
    public $reg_time = 0;

    /**
     *
     * @var string
     */
    public $last_ip = '0.0.0.0';

    /**
     *
     * @var integer
     */
    public $last_time = 0;

    /**
     *
     * @var integer
     */
    public $last_failed = 0;

    /**
     *
     * @var string
     */
    public $failed_nums = 0;

    /**
     * 状态(NORMAL:正常,LOCKED:锁定,APPLY:申请账号,REJECT:申请被拒绝,LEAVE:离职)
     *
     * @var string
     */
    public $status = 'NORMAL';

    /**
     *
     * @var integer
     */
    public $creator = 0;

    /**
     *
     * @var integer
     */
    public $ctime = 0;

    /**
     *
     * @var integer
     */
    public $mtime = 0;

    // 禁止删除操作
    public function delete()
    {
        return false;
    }

    // 根据 user_id 查找用户
    public static function findFirstByUserId($user_id)
    {
        static $cached = array();

        if (! $user_id) return false;

        if (! isset($cached[$user_id])) {
            $cached[$user_id] = self::findFirst('user_id=' . intval($user_id));
        }

        return $cached[$user_id];
    }

    // 获取用户的真实姓名
    public static function getRealName($user)
    {
        if (is_numeric($user)) {
            $user = self::findFirstByUserId($user);
        }

        is_object($user) && $user = $user->toArray();

        if (! empty($user)) {
            if ($user['language'] === 'zh-cn') {
                return $user['last_name'] . $user['first_name'];
            }

            return $user['first_name'] . ' ' . $user['last_name'];
        }

        return null;
    }

    // 获取用户的部门记录
    public static function getDepartments($user_id, $field = null)
    {
        if (! $rows = OaDepartmentUsers::find('user_id=' . $user_id)) {
            return false;
        }

        $departments = OaDepartments::getAll();
        $result = array();
        foreach ($rows as $row) {
            if ($dept = array_get($departments, $row->dept_id)) {
                $result[] = $dept;
            }
        }

        return $field === null ? $result : array_pick($result, $field);
    }

    // 获取用户的角色记录
    public static function getRoles($user_id, $field = null)
    {
        if (! $rows = OaUserRoles::find('user_id=' . $user_id)) {
            return false;
        }

        $roles = OaRoles::getAll();
        $result = array();
        foreach ($rows as $row) {
            if ($role = array_get($roles, $row->role_id)) {
                $result[] = $role;
            }
        }

        return $field === null ? $result : array_pick($result, $field);
    }

    public static function getFilterBuilder(array $filter = array())
    {
        $builder = self::getInstance()->createBuilder();

        if ($email = array_get($filter, 'email')) {
            $builder->andWhere("email like '%$email%'");
        }

        if ($reg_time_from = array_get($filter, 'reg_time_from')) {
            if (is_date($reg_time_from)) {
                $reg_time_from .= ' 00:00:00';
                $builder->andWhere("reg_time >= " . strtotime($reg_time_from));
            }
        }

        if ($reg_time_to = array_get($filter, 'reg_time_to')) {
            if (is_date($reg_time_to)) {
                $reg_time_to .= ' 23:59:59';
                $builder->andWhere("reg_time <= " . strtotime($reg_time_to));
            }
        }

        if ($creator = array_get($filter, 'creator')) {
            $builder->andWhere('creator=' . $creator);
        }

        if ($status = array_get($filter, 'status')) {
            $builder->andWhere("status='$status'");
        }

        if ($dept_id = array_get($filter, 'dept_id')) {
            $childs = array_keys(OaDepartments::getAllChilds($dept_id));
            array_push($childs, $dept_id);
            $builder
                ->columns('OaUsers.*')
                ->join('OaDepartmentUsers', 'd.user_id=OaUsers.user_id', 'd')
                ->inWhere('d.dept_id', $childs)
                ->groupBy('OaUsers.user_id');
        }

        if ($role_id = array_get($filter, 'role_id')) {
            $builder
                ->columns('OaUsers.*')
                ->join('OaUserRoles', 'r.user_id=OaUsers.user_id', 'r')
                ->andWhere('r.role_id=' . $role_id)
                ->groupBy('OaUsers.user_id');
        }

        return $builder;
    }
}
