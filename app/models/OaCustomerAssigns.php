<?php

class OaCustomerAssigns extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $assign_id;

    /**
     *
     * @var string
     */
    public $description = '';

    /**
     *
     * @var integer
     */
    public $creator = 0;

    /**
     *
     * @var integer
     */
    public $customers = 0;

    /**
     *
     * @var integer
     */
    public $ctime = 0;

    public function beforeValidation()
    {
        if (! trim($this->description)) {
            return $this->errorMessage('Batch name is required');
        }

        if ($find = self::findFirst("description='{$this->description}'")) {
            if ($find->assign_id != $this->assign_id) {
                return $this->errorMessage('Batch name already exists');
            }
        }

        parent::beforeValidation();
    }

    // 更新统计数量
    public static function updateCustomerNums($assign_id)
    {
        if ($model = self::findFirst('assign_id=' . $assign_id)) {
            $model->customers = OaCustomers::count('assign_id=' . $assign_id);
            $model->update();
        }
    }

}
