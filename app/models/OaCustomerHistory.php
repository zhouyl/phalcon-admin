<?php

class OaCustomerHistory extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $operator = 0;

    /**
     *
     * @var string
     */
    public $old_data = '';

    /**
     *
     * @var string
     */
    public $new_data = '';

    /**
     *
     * @var string
     */
    public $remark = '';

    /**
     *
     * @var integer
     */
    public $ctime = 0;

}
