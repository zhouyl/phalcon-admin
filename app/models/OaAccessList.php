<?php

class OaAccessList extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $role_id;

    /**
     *
     * @var string
     */
    public $resource;

    /**
     *
     * @var integer
     */
    public $allowed = 0;

}
