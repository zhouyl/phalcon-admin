<?php

class OaTranslates extends \Formax\Model
{

    /**
     *
     * @var string
     */
    public $language;

    /**
     *
     * @var string
     */
    public $keyword;

    /**
     *
     * @var integer
     */
    public $foreign_id = 0;

    /**
     *
     * @var string
     */
    public $translate = '';

    /**
     * 已缓存数据
     *
     * @var array
     */
    protected static $cached = array();

    /**
     * 格式化并统一关键字
     *
     * @param  string $keyword
     * @return string
     */
    public static function formatKeyword($keyword)
    {
        return strtolower(str_replace(array('/', '-', '_', ' '), '.', $keyword));
    }

    /**
     * 一次性加载指定的语言到缓存
     *
     * @param string $keyword
     */
    public static function import($keyword, $language = null)
    {
        if ($language === null) {
            $language = service('i18n')->getDefault();
        }

        $keyword = self::formatKeyword($keyword);

        if (! isset(self::$cached[$language][$keyword])) {
            foreach (self::find("keyword='{$keyword}'") as $row) {
                self::$cached[$language][$keyword][$row->foreign_id] = $row->translate;
            }
        }
    }

    /**
     * 获得翻译结果
     *
     *     \OaTranslates::read('department', 16, 'zh-cn'); // 深圳
     *
     * @param  string  $keyword
     * @param  integer $foreign_id
     * @param  string  $language
     * @return string
     */
    public static function read($keyword, $foreign_id, $language = null)
    {
        if (! ($keyword && $foreign_id)) return null;

        if ($language === null) {
            $language = service('i18n')->getDefault();
        }

        $keyword = self::formatKeyword($keyword);

        if (! isset(self::$cached[$language][$keyword])) {
            self::$cached[$language][$keyword] = array();
        }

        // 从缓存中加载
        $cached = & self::$cached[$language][$keyword];

        if (! isset($cached[$foreign_id])) {
            $model = self::findFirst("language='{$language}' AND foreign_id={$foreign_id} AND keyword='{$keyword}'");

            $cached[$foreign_id] = $model ? $model->translate : '';
        }

        return $cached[$foreign_id];
    }

    /**
     * 保存翻译结果
     *
     * @param  string  $keyword
     * @param  integer $foreign_id
     * @param  string  $translate
     * @param  string  $language
     * @return boolean
     */
    public static function write($keyword, $foreign_id, $translate, $language = null)
    {
        if (! ($keyword && $foreign_id && $translate)) return null;

        if ($language === null) {
            $language = service('i18n')->getDefault();
        }

        // 英文内容不写入
        if ($language === 'en-us') {
            return false;
        }

        $keyword = self::formatKeyword($keyword);

        // 保存到缓存
        self::$cached[$language][$keyword][$foreign_id] = $translate;

        $model = new self;

        return $model->save(array(
            'language'   => $language,
            'keyword'    => $keyword,
            'foreign_id' => (integer) $foreign_id,
            'translate'  => $translate,
        ));
    }
}
