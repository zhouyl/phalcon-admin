<?php

class OaCustomerFollowLogs extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $log_id;

    /**
     *
     * @var integer
     */
    public $customer_id = 0;

    /**
     *
     * @var integer
     */
    public $follow_user_id = 0;

    /**
     *
     * @var string
     */
    public $follow_mode = 'other';

    /**
     *
     * @var string
     */
    public $description = '';

    /**
     *
     * @var string
     */
    public $screenshot = '';

    /**
     *
     * @var integer
     */
    public $ctime = 0;

    public function beforeCreate()
    {
        if (! $this->follow_user_id) {
            $this->follow_user_id = service('auth')->user_id;
        }

        parent::beforeCreate();
    }

    public function beforeValidation()
    {
        if (! $this->customer_id) {
            return $this->errorMessage('"customer_id" is required');
        }

        if (! trim($this->description)) {
            return $this->errorMessage('Follow-up result is required');
        }

        if (is_array($this->follow_mode)) {
            $this->follow_mode = implode(',', $this->follow_mode);
        }

        parent::beforeValidation();
    }

}
