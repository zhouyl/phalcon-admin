<?php

class OaCustomerDetails extends \Formax\Model
{

    /**
     *
     * @var integer
     */
    public $customer_id;

    /**
     *
     * @var string
     */
    public $first_name = '';

    /**
     *
     * @var string
     */
    public $last_name = '';

    /**
     *
     * @var integer
     */
    public $gender = 0;

    /**
     *
     * @var string
     */
    public $address = '';

    /**
     *
     * @var string
     */
    public $company = '';

    /**
     *
     * @var string
     */
    public $industry = '';

    /**
     *
     * @var double
     */
    public $balance = 0.0;

    /**
     *
     * @var integer
     */
    public $margin_level = 0;

    /**
     *
     * @var integer
     */
    public $expected_ib = 0;

    /**
     *
     * @var string
     */
    public $im_type = '';

    /**
     *
     * @var string
     */
    public $im_account = '';

    /**
     *
     * @var string
     */
    public $description = '';

}
