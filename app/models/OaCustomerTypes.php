<?php

OaTranslates::import('customer.type');

use Phalcon\Mvc\Model\Behavior\SoftDelete;

class OaCustomerTypes extends \Formax\Model
{

    const DELETED = 1;
    const NOT_DELETED = 0;

    /**
     *
     * @var integer
     */
    public $type_id;

    /**
     *
     * @var string
     */
    public $type_name = '';

    /**
     *
     * @var integer
     */
    public $sort_order = 99;

    /**
     *
     * @var integer
     */
    public $deleted = 0;

    /**
     *
     * @var integer
     */
    public $ctime = 0;

    /**
     *
     * @var integer
     */
    public $mtime = 0;

    public function initialize()
    {
        $this->addBehavior(new SoftDelete(
            array(
                'field' => 'deleted',
                'value' => self::DELETED
            )
        ));
    }

    public static function getAll()
    {
        static $cached = null;

        if ($cached === null) {
            foreach (self::find('deleted = 0') as $row) {
                $cached[$row->type_id] = $row->toArray();
            }
        }

        return $cached;
    }

    public static function getName($type_id, $lang = null)
    {
        if ($lang === null) {
            $lang = service('i18n')->getDefault();
        }

        $all = self::getAll();

        if (! isset($all[$type_id])) return null;
        return db_translate($all[$type_id]['type_name'], 'customer.type', $type_id, $lang);
    }

}
