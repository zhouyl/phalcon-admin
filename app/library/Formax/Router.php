<?php

namespace Formax;

class Router extends \Phalcon\Mvc\Router
{

    public function add($pattern, $paths = NULL, $httpMethods = null)
    {
        $route = parent::add($pattern, $paths, $httpMethods);

        // user-login => UserLoginController
        $route->convert('controller', function ($controller) {
            return str_replace('-', '_', $controller);
        });

        // page-list => pageListAction
        $route->convert('action', function ($action) {
            return camel(str_replace('-', '_', $action));
        });

        return $route;
    }

}
