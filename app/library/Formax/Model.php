<?php
namespace Formax;

class Model extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->setup(array(
            'notNullValidations' => false, // 禁止非空验证
        ));

        $defineDatabase = get_called_class() . '::DATABASE';
        $defineTable    = get_called_class() . '::TABLE';
        $definePrefix   = get_called_class() . '::PREFIX';

        // 设置数据库名
        if (defined($defineDatabase)) {
            $this->setSchema(constant($defineDatabase));
        }

        // 设置表名
        if (defined($defineTable)) {
            $this->setSource(constant($defineTable));
        }

        // 设置表名前缀
        if ($prefix = defined($definePrefix) ? constant($definePrefix) : config('database.prefix')) {
            $this->setSource($prefix . (defined($defineTable) ? constant($defineTable) : $this->getSource()));
        }
    }

    public function beforeValidation()
    {
        foreach ($this->toArray() as $key => $value) {
            if ($this->{$key} === '') {
                // 将为空或null的值，转换为空值
                $this->{$key} = new \Phalcon\Db\RawValue("''");
            }
        }
    }

    public function beforeCreate()
    {
        if (isset($this->ctime)) {
            $this->ctime = time();
        }

        if (isset($this->mtime)) {
            $this->mtime = time();
        }
    }

    public function beforeUpdate()
    {
        if (isset($this->ctime) && ! $this->ctime) {
            $this->ctime = time();
        }

        if (isset($this->mtime)) {
            $this->mtime = time();
        }
    }

    public function errorMessage($message, $field = null, $type = null)
    {
        $this->appendMessage(new \Phalcon\Mvc\Model\Message(__($message), $field, $type));

        return false;
    }

    public function getMessage()
    {
        return $this->getFirstMessage();
    }

    // 获得第一条错误消息
    public function getFirstMessage()
    {
        if (count($this->getMessages())) {
            return (string) current($this->getMessages());
        }

        return false;
    }

    // 获取最后一条错误消息
    public function getLastMessage()
    {
        if (count($this->getMessages())) {
            return (string) end($this->getMessages());
        }

        return false;
    }

    public function getProfiler()
    {
        return service('dbListener')->getProfiler();
    }

    public function getLastProfile()
    {
        return $this->getProfiler()->getLastProfile();
    }

    // 获得最后一条 SQL
    public function getLastSql(array $params = null)
    {
        return pdo_statement_to_sql($this->getLastProfile()->getSQLStatement(), $params);
    }

    // 创建一个查询
    public function createBuilder()
    {
        return $this->getModelsManager()->createBuilder()->from(get_called_class());
    }

    // 获取单例
    public static function getInstance()
    {
        static $instance = null;

        if ($instance === null) {
            $class = get_called_class();
            $instance = new $class;
        }

        return $instance;
    }

}
