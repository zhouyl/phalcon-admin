<?php

namespace Formax\Uploader\Rename;

use Formax\Uploader\RenameInterface as RenameInterface;

class Md5 implements RenameInterface
{

    /**
     * 目录级数
     *
     * @var integer
     */
    protected $_level = 1;

    /**
     * 构造方法
     *
     * @param integer $level
     */
    public function __construct($level = 1)
    {
        $this->_level = (integer) $level;
    }

    /**
     * 文件重命名
     *
     * @param  string $path
     * @param  string $file
     * @return string
     */
    public function rename($path, $file)
    {
        if (! is_file($file)) {
            return false;
        }

        // 转换为 SplFileInfo
        $fileinfo = new \SplFileInfo($file);

        // 生成新的路径及文件名
        $md5 = md5_file($file);
        $local = 0;
        $newfile = $path;
        for ($i = 0; $i < $this->_level; $i++) {
            $newfile .= '/' . substr($md5, $local, 2);
            $local += 2;
            if ($local > 30) break; // 文件名长度不足
        }
        $newfile .= '/' . $md5 . '.' . $fileinfo->getExtension();

        // 创建新目录
        $dir = dirname($newfile);
        if (! is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        // 文件重命名
        if (rename($file, $newfile)) {
            return $newfile;
        }

        return false;
    }

}
