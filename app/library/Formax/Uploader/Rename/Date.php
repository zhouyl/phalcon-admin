<?php

namespace Formax\Uploader\Rename;

use Formax\Uploader\RenameInterface as RenameInterface;

class Date implements RenameInterface
{

    /**
     * 文件重命名
     *
     * @param  string $path
     * @param  string $file
     * @return string
     */
    public function rename($path, $file)
    {
        if (! is_file($file)) {
            return false;
        }

        // 转换为 SplFileInfo
        $fileinfo = new \SplFileInfo($file);

        // 生成新的路径及文件名
        $newfile = "$path/" . date('Y-m/dHis') . rand(1000, 9999) . '.' . $fileinfo->getExtension();

        // 创建新目录
        $dir = dirname($newfile);
        if (! is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        // 重命名文件
        if (rename($file, $newfile)) {
            return $newfile;
        }

        return false;
    }

}
