<?php

namespace Formax\Uploader;

class Result extends \SplFileInfo
{

    public function getStaticPath()
    {
        return ltrim(str_replace(DOC_PATH, '', $this->getRealPath()), '/');
    }

    public function getStaticUrl()
    {
        return static_url($this->getStaticPath(), false);
    }

}
