<?php

namespace Formax\Uploader;

interface RenameInterface
{

    /**
     * 文件重命名
     *
     * @param  string $path
     * @param  string $file
     * @return string
     */
    public function rename($path, $file);
}
