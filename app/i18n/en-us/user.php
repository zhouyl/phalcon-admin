<?php

return array(
    'NORMAL' => '<font color="green">Normal</font>',
    'LOCKED' => '<font color="blue">Locked</font>',
    'APPLY'  => '<font color="orange">Applying</font>',
    'REJECT' => '<font color="red">Rejected</font>',
    'LEAVED' => '<font color="gray">Leaved</font>',
);
