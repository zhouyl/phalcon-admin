<?php

return array(
    'r.system' => 'System',
    'r.system.auth' => 'Auth',
    'r.system.auth.login' => 'Login',
    'r.system.auth.profile' => 'Profile',
    'r.system.auth.password' => 'Password',
    'r.system.user' => 'User',
    'r.system.user.list' => 'List',
    'r.system.user.search' => 'Search',
    'r.system.user.add' => 'Add',
    'r.system.user.edit' => 'Edit',
    'r.system.user.del' => 'Del',
    'r.system.department' => 'Department',
    'r.system.department.list' => 'List',
    'r.system.department.search' => 'Search',
    'r.system.department.add' => 'Add',
    'r.system.department.edit' => 'Edit',
    'r.system.department.del' => 'Del',
);