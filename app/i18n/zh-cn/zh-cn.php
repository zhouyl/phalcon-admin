<?php

return array(
    'Formax OA System' => 'Formax-OA 系统',
    'Home' => '首页',
    'Dashboard' => '控制台',
    'THEME COLOR' => '风格定制',
    'Layout' => '布局',
    'Fluid' => '宽屏',
    'Boxed' => '窄屏',
    'Header' => '头部',
    'Fixed' => '固定',
    'Scroll' => '滚动',
    'Sidebar' => '侧栏',
    'Sidebar Position' => '侧栏位置',
    'Left' => '左',
    'Right' => '右',
    'Footer' => '底部',
    'My Profile' => '个人资料',
    'My Setting' => '偏好设置',
    'My Inbox' => '我的消息',
    'My Tasks' => '我的任务',
    'Full Screen' => '全屏',
    'Lock Screen' => '锁屏',
    'Log Out' => '退出',
    'No Title' => '无标题',
    'Quick Links' => '快速导航',

    // 菜单
    'System Settings' => '系统设置',
    'Customers Listing' => '客户管理',
        'All Customers' => '所有客户',
        'My Customers' => '我的客户',
        'Assigned' => '已分配客户',
        'No Follow-up' => '未跟进客户',
        'To Be Assigned' => '待分配客户',
    'Access Management' => '权限管理',
        'Users Listing' => '用户管理',
        'Departments Listing' => '部门列表',
        'Roles Setting' => '角色设置',
        'Resources Setting' => '资源设置',
        'Access Control' => '访问控制',

    // 常用文本
    'User' => '用户',
    'Users' => '用户',
    'Role' => '角色',
    'Roles' => '角色',
    'Customer' => '客户',
    'Customers' => '客户',
    'Department' => '部门',
    'Departments' => '部门',

    // 常见操作
    'Actions' => '操作',
    'Status' => '状态',
    'Add' => '新增',
    'New' => '增加',
    'Append' => '追加',
    'Insert' => '插入',
    'Create' => '创建',
    'Edit' => '编辑',
    'Follow' => '跟进',
    'Modify' => '编辑',
    'Assign' => '分配',
    'Permit' => '权限',
    'Update' => '更新',
    'Delete' => '删除',
    'Remove' => '移除',
    'Search' => '搜索',
    'Confirm' => '确认',
    'OK' => '确定',
    'Submit' => '提交',
    'Reset' => '重置',
    'Cancel' => '取消',
    'Close' => '关闭',
    'Back' => '返回',
    'System' => '系统',
    'Unkown' => '未知',
    'Lock' => '锁定',
    'Unlock' => '解锁',
    'Select...' => '请选择...',
    'Search Result' => '搜索结果',
    'Sort' => '排序',
    'Display Order' => '显示顺序',
    'Node' => '节点',
    'ASC' => 'ASC - 升序',
    'DESC' => 'DESC - 降序',
    'Unknown' => '未知',
    'Female' => '女',
    'Male' => '男',

    // 分页
    'Previous' => '上一页',
    'Next' => '下一页',
    'First' => '第一页',
    'Last' => '下一页',

    // 其它
    'Unknown Error' => '未知错误',
    'Network Error: ' => '网络错误：',
    'LOADING...' => '加载中...',
    'Sorry !' => '抱歉！',
    'No Data' => '暂无数据',
    'Click to' => '点击这里',
    'No records found!' => '未找到任何记录',
    'Are you sure you want to delete this record?' => '您确定要删除该条记录吗？',
    'The larger the number the more front' => '数字越大，显示越靠前',
    'Invalid sort number' => '无效的排序值',
    'Total :total records' => '共 :total 条记录',
    'Page :current of :pages, Found total :total records' => '共找到 :total 条记录，:current/:pages 页',
    'The record not found' => '未找到相关记录',

    // js dialog
    'Confirm ?' => '确认？',
    'Success !' => '操作成功！',
    'Error !' => '发生错误！',
    'Warning !' => '警告！',
    'Message !' => '提示信息！',
);
