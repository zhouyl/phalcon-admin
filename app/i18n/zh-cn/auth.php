<?php

return array(
    'Email is required.' => '邮箱不能为空',
    'Password is required.' => '密码不能为空',
    'Invalid login account.' => '无效的登录帐号',
    'Account has been locked, please contact the administrator.' => '账号已被锁定，请联系管理员',
    'Account has been locked, please try again after :minute minutes.' => '帐号已被锁定，请在:minute分钟后重试',
    'Account or password error.' => '账号或密码错误',
    'Account or password error, you can also try :limit times.' => '账号或密码错误，您还可以重试:limit次',
);
