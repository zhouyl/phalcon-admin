<?php

return array(
    'r.system' => '系统相关',
    'r.system.auth' => '登录认证',
    'r.system.auth.login' => '登录',
    'r.system.auth.profile' => '修改资料',
    'r.system.auth.password' => '修改密码',
    'r.system.user' => 'User',
    'r.system.user.list' => 'List',
    'r.system.user.search' => 'Search',
    'r.system.user.add' => 'Add',
    'r.system.user.edit' => 'Edit',
    'r.system.user.del' => 'Del',
    'r.system.department' => 'Department',
    'r.system.department.list' => 'List',
    'r.system.department.search' => 'Search',
    'r.system.department.add' => 'Add',
    'r.system.department.edit' => 'Edit',
    'r.system.department.del' => 'Del',
);