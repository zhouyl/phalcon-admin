<?php

return array(
    'Add Role' => '新增角色',
    'Edit Role' => '编辑角色',
    'Staff' => '成员',
    'Department' => '所属部门',
    'Description' => '角色说明',
    'This role is protected can not be deleted' => '该角色受到保护，禁止删除！',
);
