<?php

return array(
    'Users Search' => '用户搜索',
    'Add User'     => '新增用户',
    'Real Name'    => '姓名',
    'Register At'  => '注册时间',
    'Last Visited' => '上次访问',
    'Leaved'       => '离职',
    'NORMAL'       => '<font color="green">正常</font>',
    'LOCKED'       => '<font color="blue">已锁定</font>',
    'APPLY'        => '<font color="orange">申请中</font>',
    'REJECT'       => '<font color="red">拒绝申请</font>',
    'LEAVED'       => '<font color="gray">已离职</font>',
);
