<?php

return array(
    'Page Profiler'     => '页面分析',
    'Page Time:'        => '页面加载时间：',
    'secs'              => '秒',
    'Usage Memory:'     => '消耗内存：',
    'Database Profiler' => '数据库分析',
    'Total Queries:'    => '总查询次数：',
    'Elapsed Time:'     => '消耗时间：',
    'SQL Queries'       => 'SQL 查询语句',
);
