<?php

return array(
    'User Login' => '用户登录',
    'Login to your account' => '用户登录',
    'Enter any username and password.' => '请输入用户名和密码。',
    'Username' => '用户名',
    'Username or Email' => '用户名或密码',
    'Password' => '密码',
    'Remember me' => '保存密码(一周)',
    'Login' => '登录',
    'Forgot your password ?' => '忘记密码？',
    'no worries, click #here to reset your password.' => '点击#here重置您的登录密码。',
    'here' => '这里',
    'Don\'t have an account yet ?' => '还没有登录账号？',
    'Create an account' => '申请创建登录帐号',
    'Forget Password ?' => '忘记密码？',
    'Enter your e-mail address below to reset your password.' => '请输入您的邮件地址，以便发送密码重置邮件。',
    'Email' => '您的邮件地址',
    'Email is required.' => '邮件地址不能为空',
    'Invalid e-mail address.' => '无效的邮件地址',
);
