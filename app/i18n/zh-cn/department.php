<?php

return array(
    'Dept Name' => '部门名称',
    'Parent Dept' => '上级部门',
    'Staff' => '成员',
    'Add Department' => '新增部门',
    'Edit Department' => '编辑部门信息',
    'Dept name is required' => '部门名称不能为空',
);
