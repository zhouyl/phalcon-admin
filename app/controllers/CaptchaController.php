<?php

class CaptchaController extends ControllerBase
{

    public function indexAction()
    {
        $this->view->disable();

        // 获取配置
        $config = config('application.captcha');

        $width  = $this->request->get('width', null, $config->width);
        $height = $this->request->get('height', null, $config->height);

        captcha_output(null, $width, $height);

        exit; // 退出，以避免被外部添加内容
    }

}
