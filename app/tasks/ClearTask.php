<?php

class ClearTask extends \Formax\CLI\Task
{

    public function registerOptions()
    {
        $this
            ->registerOption('all',      'a', false, false, '清除所有 data')
            ->registerOption('metadata', 'm', false, false, '清除 metadata')
            ->registerOption('cache',    'c', false, false, '清除 cache')
            ->registerOption('logs',     'l', false, false, '清除 logs');
    }

    public function mainAction()
    {
        if ($this->args->all || $this->args->isEmpty()) {
            $this->clearAll();
        } else {
            if ($this->args->metadata) $this->clearMetaData();
            if ($this->args->cache)    $this->clearCache();
            if ($this->args->logs)     $this->clearLogs();
        }
        cli_output('done!', 'green');
    }

    public function clearAll()
    {
        $this->clearMetaData();
        $this->clearCache();
        $this->clearLogs();
    }

    public function clearMetaData()
    {
        $shell = 'rm -rf ' . ROOT_PATH . '/data/metadata/*';
        shell_exec($shell);
        cli_output($shell);
    }

    public function clearCache()
    {
        $shell = 'rm -rf ' . ROOT_PATH . '/data/cache/*';
        shell_exec($shell);
        cli_output($shell);
    }

    public function clearLogs()
    {
        $shell = 'rm -rf ' . ROOT_PATH . '/data/logs/*';
        shell_exec($shell);
        cli_output($shell);
    }

}
