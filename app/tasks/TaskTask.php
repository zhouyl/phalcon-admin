<?php
/**
 * 自动创建新的 CLI Task
 *
 * @cli-usage task --name=<task> --extends=<class>
 */
class TaskTask extends \Formax\CLI\Task
{

    public function registerOptions()
    {
        $this
            ->registerOption('name', 'n', true, true, '创建 Task，指定 Task 名称')
            ->registerOption('base', 'b', false, true, '继承的父类名，默认为：\Formax\CLI\Task');
    }

    public function mainAction()
    {
        $name  = camel($this->args->name, true);
        $class = $name . 'Task';
        $file  = APP_PATH . '/tasks/' . $class . '.php';
        $base  = $this->args->get('base', '\Formax\CLI\Task');

        if (is_file($file)) {
            cli_error('Task 已存在：' . strip_path($file));
        }

        if (! class_exists($base)) {
            cli_error("父类 '$base' 不存在");
        }

        $code = <<<CODE
<?php

class $class extends $base
{

    public function registerOptions()
    {}

    public function mainAction()
    {
        // do some thing ...
    }

}
CODE;

        if (! file_put_contents($file, $code)) {
            cli_error('Task 写入失败：$file' . strip_path($file));
        }

        cli_output('Task 创建成功：' . strip_path($file), 'success');
    }

}
