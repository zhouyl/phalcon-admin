# 环境设置

## Nginx 配置:

```
server {
    listen      80;
    server_name crm.eformax.com;
    root        /data/vhosts/crm.eformax.com/public;
    charset     utf-8;
    index       index.php index.html index.htm;

    try_files   $uri $uri/ @rewrite;

    location @rewrite {
        rewrite ^/(.*)$ /index.php?_url=/$1;
    }

    location ~ \.php$ {
        fastcgi_pass            127.0.0.1:9000;
        fastcgi_index           index.php;
        include                 fastcgi_params;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_param           SCRIPT_FILENAME    $document_root$fastcgi_script_name;
        fastcgi_param           PATH_INFO          $fastcgi_path_info;
        fastcgi_param           PATH_TRANSLATED    $document_root$fastcgi_path_info;
        fastcgi_param           APP_ENV            'PRODUCTION'; # PRODUCTION|TESTING|DEVELOPMENT
    }

    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|ico|js|css)$ {
        expires    30d;
        access_log off;
    }

    location ~ /\.ht {
        deny all;
    }

    access_log  logs/crm.eformax.com.log  main;
}
```

# 项目安装

## 自动安装运行环境

```bash
sh bin/install.sh
```

将自动安装 composer & phalcon-devtools，以及项目相关 composer packages，更多 composer 相关信息，请参考：<https://packagist.org/>

### 项目初始化

初次使用，请在调用 install.sh 后，运行项目初始化脚本 bin/app-init.sh

## Node.js组件

- Require.js <http://www.requirejs.org/>
- CoffeeScript <http://coffeescript.org/>
- UglifyJS <https://github.com/mishoo/UglifyJS>
- Grunt <http://gruntjs.com/>
- JSHint <http://www.jshint.com/>

```bash
npm install -g requirejs
npm install -g cake
npm install -g uglify-js
npm install -g jshint
npm install -g csso
```
